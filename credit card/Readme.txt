Fabio D'Adda matricola: 817279
Mario Enrico Centrone matricola: 816325

USEKERNEL -> Se TRUE viene usata una kernel density, ossia metodo non parametrico utilizzato per il riconoscimento di pattern e per la classificazione attraverso una stima di densità negli spazi metrici, o spazio delle feature.
ADJUST -> Ci permette di fare alcuni aggiustamenti alla bandwith del kernel.
LAPLACE -> Tecnica utilizzata per smussare i dati nell'applicazione di una funzione di filtro il cui scopo è evidenziare i pattern significativi.


GAMMA -> Il parametro gamma nel kernel RBF determina la portata di una singola istanza di addestramento. Se il valore di Gamma è basso, ogni istanza di addestramento avrà una vasta portata. Al contrario, valori elevati di gamma significano che le istanze di addestramento avranno una portata ravvicinata. Quindi, con un valore di gamma elevato, il confine decisionale SVM dipenderà semplicemente dai punti più vicini al confine decisionale, ignorando di fatto i punti più lontani. In confronto, un valore di gamma basso risulterà in un confine decisionale che considererà i punti più lontani da esso. Di conseguenza, valori elevati di gamma in genere producono limiti decisionali altamente flessibili e valori bassi di gamma spesso si traducono in un confine decisionale più lineare
C -> Parametro che regola l'ampiezza del margine. Un valore grande di C stringe il margine e un valore piccolo lo allarga.


THRESHOLD -> La curva ROC permette anche di identificare il valore soglia ottimale (il cosiddetto best cut-off), cioè il valore del test che massimizza la differenza tra i veri positivi e i falsi positivi.